# The introduction to Atari 2600 BankSwitching you never asked for

The introduction I wish I could find about coding a ROM with banswtching for
the Atari 2600.

Author : Fabien Tregan
License : Do what the fuck you want with this. I'm not responsible.

If you want you can send me a postcard to: 
Fabien Tregan, Les Alberts, 81310 Lisle sur Tarn, France.

## What is Bank Switching ?

The atari 2600 has a processor which internataly use 16bit adressing at the
opcode level (because it is based on the 6502), but has only 13bit effectively
connected to the adress bus.

To help keeping cartridges safe and cheap (more on that later), one of this bits
on the address bus is used to tell weither you want to adress the ROM in the
cartridge or the other components (TIA or RIOT). Hence you have only 12bit to
tell which byte of the ROM you want to read, allowing to address 2^12=4096bytes.

For some applications, this might not be enough. ROM became cheaper and bigger
game were designed making developers want to overcome this limitation.

The 2600 port Atari's arcade game "Asteroids" used a trick that is known as
Bankswitching: two of the possible addresses are used to control a "switch"
which will toggle the active half of the internal address space of the rom.
The other 4094 adresses will then allow the CPU to read any of the 4094 of the
currently selected "bank", almost doubling the amount of data it can access.

Later verson of this trick used more than two banks, eentually used more
complexe schemes (e.g. the first 2k of the cartridge address space could allow
accessing a different bank than the higher 2k, the bank switching could give
access to either more ROM or to some RAM, ...)

## What will we learn here ?

We will not learn to code for the Atari 2600, the reader is expected to already
have coded some things for the Atari 2600, hae at least basic knowledge of Dasm
macro assembler, and came to a point wher she or he need more RAM or ROM.

We will write some code for the Atari 2600 using dasm macro assembler that can
run on an "F8" bankswitching cartridge (the simpler one), then for one of the 
most complicated one : the "E7" scheme (that allow for 11.5k of ROM and 2K of
RAM)

## Before we start: ORG, RORG, and content of the ROM file


### The ORG peudo-op

When we assemble some assembly file, e.g.:
```asm
	processor	6502

	seg	code
	org 0

	dc 4, 5, 6, 7

	nop
	nop
	nop
```

we use the `processor` pseudo-op to tell we the assembly to generate binary
for the 6502 (the VCS actully has a 6507 but the assembly doesn't know about the
6507 and will assemble thinking it's for a 6502 which is ok)

we then declare a /segment/ using the `seg` pseudo-op (here we name it `code`
but anything will do for VCS dev)

we then use an `org` pseudo-op to tell the assembly where in the address space
the next thing to assemble will be. It tells the assembler the current ORiGine
of the code (or data) 

then, in this example, we use the `dc` pseudo-op to put arbitrary numbers that
will be directly put in the binary (without assembling anything) and finally a
few instructions (here `nop`s) which will be assembled in this order. Opcode for
`nop` ie `$EA` so if we assemble :

```
dasm.exe "a.asm" -obuild/a.rom -f3
```

and use an Hex viewer to see what is inside our `a.rom`file we find:

```
  Offset: 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 	
00000000: 04 05 06 07 EA EA EA                               ....jjj
```

No surprise, starting from the beginning of the file, we find our numbers then 
the opcodde for the three `nop` instructions.

If we use a different `org`, e.g. $1000 we get... exactly the same result. Why?
Because though the assembly knows this segment will start at $1000, it is most
probably not because you want to skip $1000 (i.e. 4096) byte before your code,
but because this code will be put at some place (a ROM in the case of the VCS)
that will be seen by the CPU startng at address $1000.

So using a `org $1000` we can tell the assembly were the code will be put, but
it will generate a `.rom` file that starts with what will be put at address
$1000. This way when you want to flash your rom, you can just put the first byte
of the rom file in the first byte of the rom and so on.

Then why is the use of this `.org`? It serves two purposes.

#### Setting the position counter

First, we assemble
this code:
```asm
	processor	6502

	seg	code
	org 0

start
	nop
	jmp start

	dc $FF, $FF
```
we get the following binary file:
```
  Offset: 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 	
00000000: EA 4C 00 00 FF FF                                  jL....
```
`$EA` is our `nop`, $4C is the beginning of the `jump` opcode, then the `$0000`
is the end of the opcode telling the processor to jump to address `$0000`.

If instead we used a `org $1234` we get:
```
  Offset: 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 	
00000000: EA 4C 34 12 FF FF                                  jL4...
```
so even though the assembly put the opcode at the same place in the binary file,
the intenal position counter has been set differently: it know the instructions
will be assembled at different places and can calculate a different value for
the `start` label, generate the opcode to jump at `$1234` (which will be $34 
followed by $12 in the rom, search for "little endian" if you want to know more)

#### Filling the gaps

We showerthat The ```org``` pseudo-op does not change where in the binary file
the code or ata will be put, but this is only true for the first `org` of the
segment. If we put a second one, the gap will be filled with a default or a
given value. e.g.:
```asm
	processor	6502

	seg	code
	org $1000

	dc $01, $02, $03

	org $1020,$AA

	dc $AB, $CD, $EF
```
will be assembled into:
```
  Offset: 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 	
00000000: 01 02 03 AA AA AA AA AA AA AA AA AA AA AA AA AA    ...*************
00000010: AA AA AA AA AA AA AA AA AA AA AA AA AA AA AA AA    ****************
00000020: AB CD EF                                           +Mo

```
The rom file will start with the $01, $02 and $03 which should be seen by the
CPU at address $1000, then it's filled wth $AA until we reach what wll be seen
at $1020, i.e. the $AB, $CD nd $EF.

Make sure you read this until it's very clear to you, that will make things
easyer later :)

### the RORG pseudo-op

Now we know how the `org` pseudo-opp is used both to tell the assemly where the
code will be with respect to the CPU (so the assembly can calculate values for
the labels) and to tell it where in the binary file the assembled instruction
(or data) must be put.

But using bankswitching,wehave a problem: Imagine we have to different pieces of
code at the beginning of two different banks. Those two pieces of code will be 
seen at the same address by the CPU - its the catridge which will decide which
one it gies to the CPU depending on the currently active bank.

But since the two pieces of code are at the same address, what we know about
`org` means they will be at the same position in the binary file, which is not 
possible!

On possibiity would be to use to different `.asm` file, compile them separately
and generate to separate bnary file,burn each one on a different ROM chip. But
this is not what we will do.

Let me introduce to you the last pseudo-op we need: `rorg`. `rorg` will Relocate
the ORiGine. Though the assembly will still use the origine given by `org`, it
will use the reallocated origin to calculate the labels.

Hence this code:
```asm
	processor	6502

	seg	code
	org $1000

start
	jmp start

	org $1020,$AA
	rorg $1000
start2
	jmp start2
```
will be ssembled into:
```
  Offset: 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 	
00000000: 4C 00 10 AA AA AA AA AA AA AA AA AA AA AA AA AA    L..*************
00000010: AA AA AA AA AA AA AA AA AA AA AA AA AA AA AA AA    ****************
00000020: 4C 00 10                                           L..
```
We see that:
- the two jumps (the `4C xx xx`) are assembled in the rom file at offsets $00
and $20 (because they will be loaded at $1000 and $1020 and there is nothing to
assemble before $1000 so $0000 to $0FFF is skipped in the binary file)
- they both jump to address $1000 because the first jumps to `start` which is
originated at $1000 and the second one jumps to `start2` which is reoriginated
at $1000

Ok, now we understand what we are doing, and playing with bank switching will be
fun and easy.


## F8 BankSwitching code.

F8 bank switching is the first and the simplest bank switching scheme on the 
VCS. It allows switching between two different banks. The first one is selected
by putting $1FF8 on the address bus, the second one by putting $1FF9 on this
bus.

Putting a value on the address but can be one by:
-Jumping / running code until we reach this address (the CPU will put the address
on the bus to fetch the instruction), this is not realy handy
-Reading some data at this adresse (using e.g. `lda`, `ora`... instructions),
this will wok but will modify some register or status bit of the cpu
-Writting some data to the address. The original Asteroids code used an `lda`
so this is what we will use.

Because we want to make a .rom file that can be read by the Stella emulator and
most development cartridge, we will :
- Name our file ending with `.F8` to tell we use F8 bank switching
- Put the first bank at the beginning of the .rom file, immediately followed by
the second bank

Also, we will use two different segments just because we can. Lets start with:

```asm
	processor	6502


;;;; Bank 0

	seg	Bank0
	org $1000

start
	jmp start


;;;; Bank 1

	seg	Bank1
	org $2000,$b0
	rorg $1000

start2
	jmp start2
```

The first bank is originated at $1000 because this is where the CPU will see it
starting. (actually is will also see it starting at $3000, $5000, $7000, $9000,
$b000, $d000 and $f000, because the two higher bits of the adress bus of the
CPU are not connected to the address but of the VCS -  the 6507 CPU of the  VCS
being a cheaper version of the 6502)

The second bank is originated at $2000 because the first one is as $1000 and we
want the second one to start 4096 ($1000) further, and $1000 + 4096 = $2000.

It is reoriginated at $1000 because the CPU will see both banks at
$1000

We could also have originated the banks at $0000 and $1000 or $1234 and $2234,
the only requirements are that:
-org of the second bank is the one first bank + the size of the first bank
-both banks are originated or reoriginated at $1000 (or $3000, $5000,...)

You also noticed that, with `org $2000,$b0`, we told the assembly to use not the
default value to fill the gap between end of Bank0 and start of Bank1, but B1.
This way, Bank0 is filled with `B1`s and is easy to recognize when using an
hex editor or the debugger in Stella.


### Clean reset

This code compiles, but it won't run, because the reset vector (at $fffc) has
not been set. The reset vector (address of first executed instruction) will be
loaded by the cpu at $fffc and $fffd (using two bytes). Since the higher bits
of the address are not plugged to the address bus of the CPU anyway, putting it
at $1ffc and $1ffd will give the same result.

But wait, we have two different banks than can provide data at this address. So
we need to put the reset vector in the bank that will be activ at reset. Since
we don't wantto make prediction about which one it will be, we will just put the
reset vector in both banks. But then we are sure the CPU will jump to the same 
address at reset, but we don't know which opcode (fom Bank0 or Bank1) will be
served to the CPU at this address. The solution is simple: we just put at both
address an instruction that will switch to Bank0. We now have:

```
	processor	6502


;;;; Bank 0

	seg	Bank0
	org $1000

start
	sta	$1ff8

	nop
	nop
	nop

	org $1ffc,$b0
	.word start	; reset vector

;;;; Bank 1

	seg	Bank1
	org $2000
	rorg $1000

start2
	lda	$1ff8

	org $2ffc,$b1
	.word start	; reset vector
```

Whatever the active bank is, the reset vector is the value of `start` (i.e. 
$1000 because it is just after the `org $1000`). The CPU will set its Program
Counter to $1000 and fetch the instruction from the current activ bank. In both
case, this will be `lda $1ff8`. If will then put $1ff8 on the address bus, which
will triggerthe Bank Switching mechanism and activate Bank0. Then when next 
instruction if fetch, we aresureit is the `nop`fom Bank0.

Also note that, whatever the active bank is, if we `jmp $1000`, we will also
have a clean start in Bank0.

### Making use ofthe extra ROM

We now can run some code, but how do we make use of the extra rom space? If we
 `sta $1fff9` we will switch to Bank1, and the CPU will try to fetch the next
instruction that this time will be from Bank1. So we would use the data in Bank1
but no longeruse the one in Bank0. So to really make use of the extra data, we
need to have some test which will make us decide if we switch bank or not.

Here is a sample:
```asm
	processor	6502

;;;; Bank 0

	seg	Bank0
	org $1000

start
	lda	$1ff8

	inc $80
	beq StayInBankOne
	lda $1ff9

StayInBankOne
	inx	

	org $1ffc
	.word start	; reset vector

;;;; Bank 1

	seg	Bank1
	org $2000
	rorg $1000

	ds.b StayInBankOne - $1000

	iny

	org $2ffc
	.word start	; reset vector
```


we start in Bank0. At some point we reach the `beq` instruction. If this one
jumps to `StayInBank0`, nothing special happens. If not, we will write at $1ff9,
and the cartridge will switch to Bank1. So the next instruction for the CPU
won't be the `inx`,but the one which is at the same offset in Bank1. We used
the `ds.b` pseudo-op to fill some space so that the `iny` is at the same offset
in Bank1 that the `inx` is in Bank0, so it will be executed.


### Don't do it this way

The way we use `ds.b` to have the `iny` at the same offset in Bank1 as the
offset of the `inx` in Bank0 is not really practical. Lot of technics gives
better results and can be choosen depending on weither you want to preserve
space,have shorter execution time, make code easyer to understand...
You could e.g. have a table at the end of each bank with an `lda` to switch to
the proper bank followed a `jmp` to go piece of code you want. By duplicating
this table in each bank you could jump to the (fixed position) entry of this
table whatever the current bank is. Discussing the different method is
out of the scope of this introduction but you may want to search the Atariage 
forum (e.g. [this](https://atariage.com/forums/topic/211864-bankswitch-per-jmpjsrrts/) )

You'll probably want to learn using macros to calculatre and generate things
for you.

### Don't access $1ff8 or $1ff9 by mistake

If we put some code or some data in the rom without taking care, they could be 
located at $1ff8 or $1ff9 (or $2ff8 / $2ff9 in ssecond for second bank).
When some unaware code will try to read them, the  CPU will put on of these
addresses on the bus and trigger an unwanted bank switching.

To avoid this, we can explicitly tell the assembly that we want some chunk of
data at these addresses:

```asm
	processor	6502

;;;; Bank 0

	seg	Bank0
	org $1000

start

	nop

	org $1ff8

SwitchToBank0
	.byte 0
SwitchToBank1
	.byte 0

	org $1ffc
	.word start	; reset vector

;;;; Bank 1

	seg	Bank1
	org $2000
	rorg $1000

	lda	SwitchToBank0

	org $2ff8
	.byte	0
	.byte	0


	org $2ffc
	.word start	; reset vector
```

In Bank1, we use an `org $1ff8` and two `.byte` pseudo-op to reserve two bytes
in the rom space.

In Bank1 we also reserve two bytes at $2ff8, to prevent the assembly or the
developper to put anything there. In this bank, if we use and `lda` to switch to
another bank, we need to use the label from Bank0 (because if we defined another
label in Bank1, it would $2ff8 instead of $1ff8 since the `org` cancels the
the effect of the previous `rorg`)


### Limitation of the F8 Bank Switching.

The F8 Bank can give accès to two banks. Each one gives access to 4096 bytes of
data (minus the two that can't be easily accessed becaus they'd trigger a bank
switching). Other bank swtching scheme use more addresses to give access to more
banks. Other use more advanced mecanisme to allow for accessing many banks 
without blocking one byte per bank in each bank. It was also more costly to
implement, hence was no the first available one.

Another big limitation of this scheme is that it is not easy for some code in a
bank to read data from another bank. You would have to switch the bank, but then
the processor would read both the data AND the next instruction from the other
bank. You still could make some code that would switch to the second bank, read 
the data and put it in a register, then come back switches back to the first 
bank and goes back to the followng instruction, but this would cost a lot of
cpu cycles. Some clever trick is implemented in other bank switching schemes: 
the address space of the cartridge is split into several slices, the banks are
smaller (to fit in a slice) and you have the ablity to switch only the bank 
that can be accessed through on of the slice. This way, you could have some code
in Bank0 that the CPU can access through Slice1 (e.g. addresses $1000 to $17FF)
and banks 1,2,3,4... where you put data that can be accessed thru slice 1 (e.g. 
at addresses $1800 to $1FFF). This is what E7 does.


## E7 BankSwitching code.



## Further reading

